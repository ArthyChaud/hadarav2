package hadara.model;

import hadara.model.cartes.Colonie;
import hadara.model.exceptions.*;
import hadara.model.cartes.CarteInitiative;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JoueurUnitTest {

    private Joueur joueur1;

    @Tag("include")
    @Test
    public void testMilitaire(){
        joueur1 = new Joueur();
        joueur1.getJauge().setMilitaire(5);
        joueur1.getJauge().addMilitaire(3);
        assertEquals(8,joueur1.getJauge().getMilitaire());
        joueur1.getJauge().addMilitaire(-2);
        assertEquals(6,joueur1.getJauge().getMilitaire());
    }

    @Tag("include")
    @Test
    public void testRevenu(){
        joueur1 = new Joueur();
        joueur1.getJauge().setRevenu(5);
        joueur1.getJauge().addRevenu(3);
        assertEquals(8,joueur1.getJauge().getRevenu());
        joueur1.getJauge().addRevenu(-2);
        assertEquals(6,joueur1.getJauge().getRevenu());
    }

    @Tag("include")
    @Test
    public void testAlimentation(){
        joueur1 = new Joueur();
        joueur1.getJauge().setAlimentation(5);
        joueur1.getJauge().addAlimentation(3);
        assertEquals(8,joueur1.getJauge().getAlimentation());
        joueur1.getJauge().addAlimentation(-2);
        assertEquals(6,joueur1.getJauge().getAlimentation());
    }

    @Tag("include")
    @Test
    public void testCulture(){
        joueur1 = new Joueur();
        joueur1.getJauge().setCulture(5);
        joueur1.getJauge().addCulture(3);
        assertEquals(8,joueur1.getJauge().getCulture());
        joueur1.getJauge().addCulture(-2);
        assertEquals(6,joueur1.getJauge().getCulture());
    }

    @Tag("exclude")
    @Test
    public void testAddColonie(){
        /* modifications apporté à la classe
        Colonie colonie = new Colonie(5,5,5,5,5,5);
        joueur1 = new Joueur();
        assertEquals(1, joueur1.addColonie(colonie));
        assertEquals(0, joueur1.addColonie(colonie));
        */
    }

    @Tag("include")
    @Test
    public void testPillerColonie() throws Exception {
        Joueur joueur = new Joueur();
        Colonie colonie = Colonie.A[0];
        joueur.addColonie(colonie, true);
        assertEquals(colonie.getPillage(), joueur.getMonnaie());
    }

    @Tag("include")
    @Test
    public void testColoniserColonie() throws Exception {
        Joueur joueur = new Joueur();
        Colonie colonie = Colonie.A[0];
        joueur.addMonnaie(colonie.getAlliance());
        joueur.addColonie(colonie, false);
        assertEquals(0, joueur.getMonnaie());
        assertEquals(colonie.getJauge().getRevenu(), joueur.getJauge().getRevenu());
        assertEquals(colonie.getJauge().getMilitaire(), joueur.getJauge().getMilitaire());
        assertEquals(colonie.getJauge().getCulture(), joueur.getJauge().getCulture());
        assertEquals(colonie.getJauge().getAlimentation(), joueur.getJauge().getAlimentation());
    }

    @Tag("include")
    @Test
    public void testAlreadyOwnedColonyException() throws Exception {
        Joueur joueur = new Joueur();
        Colonie colonie1 = Colonie.A[0];
        Colonie colonie2 = Colonie.A[1];
        joueur.addColonie(colonie1, true);
        assertThrows(AlreadyOwnedColonyException.class, () -> {
            joueur.addColonie(colonie2, true);
        });
    }

    @Tag("include")
    @Test
    public void testUnexpectedNegativeValeurException() throws Exception {
        Joueur joueur = new Joueur();
        assertThrows(UnexpectedNegativeValueException.class, () -> {
            joueur.addMonnaie(-1);
        });
    }

    @Tag("include")
    @Test
    public void testMonnaieInsufisanteException() throws Exception {
        Joueur joueur = new Joueur();
        assertThrows(MonnaieInsufisanteException.class, () -> {
            joueur.addColonie(Colonie.A[0], false);
        });
    }

    @Tag("include")
    @Test
    public void testSetCarteInitiative() {
        CarteInitiative carteInitiative = CarteInitiative.carteInitiatives[0];
        Joueur joueur = new Joueur();
        joueur.setCarteInitiative(carteInitiative);
        assertEquals(joueur.getJauge().getRevenu(), carteInitiative.getJauge().getRevenu());
        assertEquals(joueur.getJauge().getMilitaire(), carteInitiative.getJauge().getMilitaire());
        assertEquals(joueur.getJauge().getCulture(), carteInitiative.getJauge().getCulture());
        assertEquals(joueur.getJauge().getAlimentation(), carteInitiative.getJauge().getAlimentation());
    }

    @Tag("include")
    @Test
    public void ajoutMedailleArgentInvalide() throws NoMoreSlotsMedaillesArgentException, MonnaieInsufisanteException, UnexpectedNegativeValueException {
        Assertions.assertThrows(NoMoreSlotsMedaillesArgentException.class, () -> {
            Joueur joueur = new Joueur();
            joueur.addMonnaie(500);
            joueur.addMedailleArgent(1);
            joueur.addMedailleArgent(1);
            joueur.addMedailleArgent(1);
        });
    }

    @Tag("include")
    @Test
    public void pasAssezDeMonnaieArgent() throws NoMoreSlotsMedaillesArgentException, MonnaieInsufisanteException {
        Assertions.assertThrows(MonnaieInsufisanteException.class, () -> {
            Joueur joueur = new Joueur();
            joueur.addMedailleArgent(1);
        });
    }

    @Tag("include")
    @Test
    public void pasAssezDeMonnaieOr() throws NoMoreSlotsMedaillesArgentException, MonnaieInsufisanteException {
        Assertions.assertThrows(MonnaieInsufisanteException.class, () -> {
            Joueur joueur = new Joueur();
            joueur.addMedailleOr();
        });
    }

    @Tag("include")
    @Test
    public void ajoutMedailleOrInvalide() throws NoMoreSlotsMedaillesOrException, MonnaieInsufisanteException, UnexpectedNegativeValueException {
        Assertions.assertThrows(NoMoreSlotsMedaillesOrException.class, () -> {
            Joueur joueur = new Joueur();
            joueur.addMonnaie(500);
            joueur.addMedailleOr();
            joueur.addMedailleOr();
            joueur.addMedailleOr();
        });
    }

    @Tag("include")
    @Test
    public void ajoutMedailleArgent() throws NoMoreSlotsMedaillesArgentException, MonnaieInsufisanteException, UnexpectedNegativeValueException {
        Joueur joueur = new Joueur();
        joueur.addMonnaie(500);
        joueur.addMedailleArgent(1);
        joueur.addMedailleArgent(3);
        Assertions.assertEquals(1, joueur.getMedaillesArgent()[0]);
        Assertions.assertEquals(3, joueur.getMedaillesArgent()[1]);
        Assertions.assertEquals(2, joueur.getMedaillesArgent().length);
    }

    @Tag("include")
    @Test
    public void ajoutMedailleOr() throws NoMoreSlotsMedaillesOrException, MonnaieInsufisanteException, UnexpectedNegativeValueException {
        Joueur joueur = new Joueur();
        joueur.addMonnaie(500);
        Assertions.assertEquals(0, joueur.getNbrMedaillesOr());
        joueur.addMedailleOr();
        Assertions.assertEquals(1, joueur.getNbrMedaillesOr());
        joueur.addMedailleOr();
        Assertions.assertEquals(2, joueur.getNbrMedaillesOr());
    }
}
