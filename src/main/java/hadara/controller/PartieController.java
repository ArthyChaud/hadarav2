package hadara.controller;

import hadara.model.Partie;
import hadara.view.View;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;

import java.util.Arrays;

public class PartieController extends BasicController {
    @FXML
    public ImageView plateau1;
    @FXML
    public ImageView plateau2;
    @FXML
    public ImageView plateau3;
    @FXML
    public ImageView plateau4;
    @FXML
    public ImageView plateau5;

    @FXML
    public ImageView carteVert;
    @FXML
    public ImageView carteBleu;
    @FXML
    public ImageView carteJaune;
    @FXML
    public ImageView carteRouge;
    @FXML
    public ImageView carteViolet;

    @FXML
    public ImageView carteVertJoueur;
    @FXML
    public ImageView carteBleuJoueur;
    @FXML
    public ImageView carteJauneJoueur;
    @FXML
    public ImageView carteRougeJoueur;
    @FXML
    public ImageView carteVioletJoueur;

    @FXML
    public ImageView joueur;

    @FXML
    public ImageView initiative;

    @FXML
    public ImageView curseurArgent1;
    @FXML
    public ImageView curseurArgent2;
    @FXML
    public ImageView curseurArgent3;
    @FXML
    public ImageView curseurArgent4;
    @FXML
    public ImageView curseurArgent5;
    @FXML
    public ImageView curseurArgent6;
    @FXML
    public ImageView curseurArgent7;
    @FXML
    public ImageView curseurArgent8;
    @FXML
    public ImageView curseurArgent9;
    @FXML
    public ImageView curseurArgent10;

    @FXML
    public ImageView curseurMilitaire1;
    @FXML
    public ImageView curseurMilitaire2;
    @FXML
    public ImageView curseurMilitaire3;
    @FXML
    public ImageView curseurMilitaire4;
    @FXML
    public ImageView curseurMilitaire5;
    @FXML
    public ImageView curseurMilitaire6;
    @FXML
    public ImageView curseurMilitaire7;
    @FXML
    public ImageView curseurMilitaire8;
    @FXML
    public ImageView curseurMilitaire9;
    @FXML
    public ImageView curseurMilitaire10;

    @FXML
    public ImageView curseurCulture1;
    @FXML
    public ImageView curseurCulture2;
    @FXML
    public ImageView curseurCulture3;
    @FXML
    public ImageView curseurCulture4;
    @FXML
    public ImageView curseurCulture5;
    @FXML
    public ImageView curseurCulture6;
    @FXML
    public ImageView curseurCulture7;
    @FXML
    public ImageView curseurCulture8;
    @FXML
    public ImageView curseurCulture9;
    @FXML
    public ImageView curseurCulture10;

    @FXML
    public ImageView curseurAgriculture1;
    @FXML
    public ImageView curseurAgriculture2;
    @FXML
    public ImageView curseurAgriculture3;
    @FXML
    public ImageView curseurAgriculture4;
    @FXML
    public ImageView curseurAgriculture5;
    @FXML
    public ImageView curseurAgriculture6;
    @FXML
    public ImageView curseurAgriculture7;
    @FXML
    public ImageView curseurAgriculture8;
    @FXML
    public ImageView curseurAgriculture9;
    @FXML
    public ImageView curseurAgriculture10;
    @FXML
    public ImageView medaileJ1;
    @FXML
    public ImageView medaileJ2;
    @FXML
    public ImageView medaileJ3;
    @FXML
    public ImageView medaileJ4;
    @FXML
    public ImageView medaileJ5;

    public int argent = 1;
    public int militaire = 1;
    public int culture = 1;
    public int agriculture = 1;

    public PartieController() {
        super();
    }

    public PartieController(Partie model, View view) {
        super(model, view);
    }

    public void unsetCardVert() {
        carteVert.setVisible(false);
        carteVertJoueur.setVisible(true);
    }

    public void unsetCardBleu() {
        carteBleu.setVisible(false);
        carteBleuJoueur.setVisible(true);
    }

    public void unsetCardJaune() {
        carteJaune.setVisible(false);
        carteJauneJoueur.setVisible(true);
    }

    public void unsetCardRouge() {
        carteRouge.setVisible(false);
        carteRougeJoueur.setVisible(true);
    }

    public void unsetCardViolet() {
        carteViolet.setVisible(false);
        carteVioletJoueur.setVisible(true);
    }

    public void setMedaileJ1(){
        medaileJ1.setVisible(true);
    }

    public void setMedaileJ2(){
        medaileJ2.setVisible(true);
    }

    public void setMedaileJ3(){
        medaileJ3.setVisible(true);
    }

    public void setMedaileJ4(){
        medaileJ4.setVisible(true);
    }

    public void setMedaileJ5(){
        medaileJ5.setVisible(true);
    }

    public void setCursorArgent(int i){
        int[] I = new int[] {i};
        Arrays.asList(
            curseurArgent1,
            curseurArgent2,
            curseurArgent3,
            curseurArgent4,
            curseurArgent5,
            curseurArgent6,
            curseurArgent7,
            curseurArgent8,
            curseurArgent9,
            curseurArgent10
        ).forEach(c -> {
            c.setVisible(false);
            if (--I[0] == 0)
                c.setVisible(true);
        });
    }

    public void plusArgent(){
        if (argent <= 10) argent += 1;
        if (argent == 11) argent = 1;
        setCursorArgent(argent);
    }

    public void moinsArgent(){
        if (argent > 1) argent -= 1;
        setCursorArgent(argent);
    }

    public void setCursorMilitaire(int i){
        int[] I = new int[] {i};
        Arrays.asList(
                curseurMilitaire1,
                curseurMilitaire2,
                curseurMilitaire3,
                curseurMilitaire4,
                curseurMilitaire5,
                curseurMilitaire6,
                curseurMilitaire7,
                curseurMilitaire8,
                curseurMilitaire9,
                curseurMilitaire10
        ).forEach(c -> {
            c.setVisible(false);
            if (--I[0] == 0)
                c.setVisible(true);
        });
    }

    public void plusMilitaire(){
        if (militaire <= 10) militaire += 1;
        if (militaire == 11) militaire = 1;
        setCursorMilitaire(militaire);
    }

    public void moinsMilitaire(){
        if (militaire > 1) militaire -= 1;
        setCursorMilitaire(militaire);
    }

    public void setCursorCulture(int i){
        int[] I = new int[] {i};
        Arrays.asList(
                curseurCulture1,
                curseurCulture2,
                curseurCulture3,
                curseurCulture4,
                curseurCulture5,
                curseurCulture6,
                curseurCulture7,
                curseurCulture8,
                curseurCulture9,
                curseurCulture10
        ).forEach(c -> {
            c.setVisible(false);
            if (--I[0] == 0)
                c.setVisible(true);
        });
    }

    public void plusCulture(){
        if (culture <= 10) culture += 1;
        if (culture == 11) culture = 1;
        setCursorCulture(culture);
    }

    public void moinsCulture(){
        System.out.println(culture);
        if (culture > 1) culture -= 1;
        setCursorCulture(culture);
    }

    public void setCursorAgriculture(int i){
        int[] I = new int[] {i};
        Arrays.asList(
                curseurAgriculture1,
                curseurAgriculture2,
                curseurAgriculture3,
                curseurAgriculture4,
                curseurAgriculture5,
                curseurAgriculture6,
                curseurAgriculture7,
                curseurAgriculture8,
                curseurAgriculture9,
                curseurAgriculture10
        ).forEach(c -> {
            c.setVisible(false);
            if (--I[0] == 0)
                c.setVisible(true);
        });
    }

    public void plusAgriculture(){
        if (agriculture <= 10) agriculture += 1;
        if (agriculture == 11) agriculture = 1;
        setCursorAgriculture(agriculture);
    }

    public void moinsAgriculture(){
        if (agriculture > 1) agriculture -= 1;
        setCursorAgriculture(agriculture);
    }
}
