package hadara.model;

import hadara.model.exceptions.InvalidColorException;
import hadara.model.Joueur;
import hadara.model.PaquetCarte;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Assertions;


public class PaquetCarteUnitTest {

    private Joueur joueur1;

    @Tag("include")
    @Test
    public void creationPaquetCarteInvalide() throws InvalidColorException {
        Assertions.assertThrows(InvalidColorException.class, () -> {
            PaquetCarte pc = new PaquetCarte("Orange");
        });
    }

    @Tag("include")
    @Test
    public void creationPaquetCarte() throws InvalidColorException{
        PaquetCarte pc = new PaquetCarte("Jaune");
        Assertions.assertEquals("Jaune",pc.getColor());
        PaquetCarte pc2 = new PaquetCarte("Bleu");
        Assertions.assertEquals("Bleu",pc2.getColor());
        PaquetCarte pc3 = new PaquetCarte("Rouge");
        Assertions.assertEquals("Rouge",pc3.getColor());
        PaquetCarte pc4 = new PaquetCarte("Violet");
        Assertions.assertEquals("Violet",pc4.getColor());
        PaquetCarte pc5 = new PaquetCarte("Vert");
        Assertions.assertEquals("Vert",pc5.getColor());
    }



}
