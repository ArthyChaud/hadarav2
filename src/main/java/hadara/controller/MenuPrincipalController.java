package hadara.controller;

import hadara.App;
import hadara.model.Joueur;
import hadara.model.Partie;
import hadara.view.MenuPrincipalView;
import hadara.view.MenuJouerView;
import hadara.view.PartieView;
import hadara.view.View;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;

import java.util.ArrayList;
import java.util.List;

public class MenuPrincipalController extends BasicController {

  @FXML
  public Button startGame;
  public Button quitGame;

  public MenuPrincipalController() {
    super();
  }

  public MenuPrincipalController(Partie model, View view) {
    super(model, view);
  }

  public void setStartGame(){
    App application = App.getInstance();
    application.setCurrentView(new MenuJouerView());
  }

  public void setQuitGame(){
    System.exit(0);
  }

}
