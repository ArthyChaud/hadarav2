package hadara.model;

import hadara.model.cartes.Carte;
import hadara.model.cartes.Packable;
import hadara.model.exceptions.InvalidColorException;

import java.util.ArrayList;
import java.util.Collections;

public class PaquetCarte extends ArrayList<Packable> {

    // PaquetCarte extend ArrayList<Carte> donc on n'a pas besoin de définir les méthodes add, remove
    // qui sont déja défini par ArrayList<>

    public static String[] colors = {"Jaune","Bleu","Vert","Violet","Rouge"};
    public String color;

    public PaquetCarte(String color) throws InvalidColorException {
        if(isValidColor(color)) {
            this.color = color;
        }else{
            throw new InvalidColorException();
        }

    }

    private static boolean isValidColor(String color){
        for(String c: colors){
            if(c.equals(color)){
                return true;
            }
        }
        return false;
    }

    public String getColor(){
        return this.color;
    }

    public String[] getColorList(){
        return colors;
    }

    private void shuffle(){
        Collections.shuffle(this);
    }

    private Carte distribute(Joueur j){
        // à voir avec la classe Joueur
        return null;
    }

}
