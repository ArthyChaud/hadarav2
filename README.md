Projet Production d'Application 2020 : Hadara sur Java

Membres (S3-C1) :
- Ayan ADELAIDE : @ayan.adelaide13 (Product Owner)
- Arthur BEAUDOIN : @ArthyChaud (Développeur)
- Leo RUPIL-BALADDA : @CyZee (Développeur)
- Nathanaël VETTORAZZI : @Novali28 (Développeur)
- Otto HAJDU : @Raykeno (Développeur)
- Noah SOCHANDAMANDON : @SowahCore (Développeur)

Trello : https://trello.com/b/UZRgaglA/projet-produc-applic-hadara

