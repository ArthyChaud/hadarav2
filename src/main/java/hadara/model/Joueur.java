package hadara.model;

import hadara.model.cartes.CarteInitiative;
import hadara.model.cartes.Colonie;
import hadara.model.exceptions.*;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class Joueur {
    private int monnaie;
    private Jauges jauge;
    private ArrayList<Colonie> colonies;
    private CarteInitiative carteInitiative;

    // Le nbr de medailles d'or (max 2)
    private int nbrMedaillesOr;
    // Les états des medailles d'argent
    // -1 rien ; 0 revenu ; 1 militaire ; 2 culture ; 3 alimentation
    private int[] medaillesArgent;

    public Joueur() {
        monnaie = 0;
        jauge = new Jauges();
        colonies = new ArrayList<>();
        nbrMedaillesOr = 0;
        medaillesArgent = new int[]{-1, -1};
    }

    public void addMonnaie(int monnaie) throws UnexpectedNegativeValueException {
        if (monnaie < 0)
            throw new UnexpectedNegativeValueException();
        this.monnaie += monnaie;
    }

    public int getMonnaie() {
        return monnaie;
    }

    public Jauges getJauge() {
        return jauge;
    }

    public int[] getMedaillesArgent(){
        return medaillesArgent;
    }

    public int getNbrMedaillesOr(){
        return nbrMedaillesOr;
    }

    // marqeurValue correspond au type du marqeur (revenu, militaire; etc...) (0 à 3)
    public void addMedailleArgent(int marqeurValue) throws NoMoreSlotsMedaillesArgentException, MonnaieInsufisanteException {
        // Il faut vérfier qu'on est à la fin de l'âge et dans quel âge
        // Vu que les prix varient
        // Je vais mettre un faux prix jusqu'à la création de âge et la possibilité d'obtenir les prix

        if (this.medaillesArgent[0] != -1 && this.medaillesArgent[1] != -1){
            throw new NoMoreSlotsMedaillesArgentException();
        }
        int currentPrice = 5; // à modifier dépendant de l'âge (faire une methode getMedailleArgentPrice() dans une classe age?)
        int slotIndex;
        if(monnaie < currentPrice) {
            throw new MonnaieInsufisanteException();
        }else{
            monnaie = monnaie - currentPrice;
            if(this.medaillesArgent[0] == -1){
                slotIndex = 0;
            }else{
                slotIndex = 1;
            }
            this.medaillesArgent[slotIndex] = marqeurValue;
        }


    }

    public void addMedailleOr() throws NoMoreSlotsMedaillesOrException, MonnaieInsufisanteException {
        // Pour currentPrice voir les commentaires pour addMedailleArgent

        if (this.nbrMedaillesOr == 2){
            throw new NoMoreSlotsMedaillesOrException();
        }
        int currentPrice = 5; // à modifier dépendant de l'âge (faire une methode getMedailleOrPrice() dans une classe age?)
        if(monnaie < currentPrice) {
            throw new MonnaieInsufisanteException();
        }else{
            this.nbrMedaillesOr++;
        }
    }

    public CarteInitiative getCarteInitiative() {
        return carteInitiative;
    }

    public void setCarteInitiative(@NotNull CarteInitiative carteInitiative) {
        this.carteInitiative = carteInitiative;
        jauge.add(carteInitiative.getJauge());
    }

    public void addColonie(@NotNull Colonie colonie, boolean pillage) throws Exception {
        for (Colonie colony : colonies)
            if (colony.getCondition() == colonie.getCondition())
                throw new AlreadyOwnedColonyException();
        colonies.add(colonie);
        colonie.setRecto(pillage);
        if (pillage)
            monnaie += colonie.getPillage();
        else {
            if (colonie.getAlliance() > monnaie)
                throw new MonnaieInsufisanteException();
            monnaie -= colonie.getAlliance();
            jauge.add(colonie.getJauge());
        }
    }

}

