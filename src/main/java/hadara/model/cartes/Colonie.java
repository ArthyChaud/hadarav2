package hadara.model.cartes;

import hadara.model.Jauges;

public class Colonie implements Packable {
    public static Colonie[] A = new Colonie[] {
            new Colonie(3, 2, new Jauges(1, 0, 0, 1)),
            new Colonie(3, 2, new Jauges(1, 1, 0, 0)),
            new Colonie(3, 2, new Jauges(0, 0, 1, 1)),
            new Colonie(3, 2, new Jauges(0, 1, 1, 0)),
            new Colonie(3, 7, new Jauges(0, 0, 1, 0))
    };
    public static Colonie[] B = new Colonie[] {
            new Colonie(9, 5, new Jauges(2, 1, 0, 0)),
            new Colonie(9, 5, new Jauges(1, 0, 0, 2)),
            new Colonie(9, 5, new Jauges(0, 0, 2, 1)),
            new Colonie(9, 5, new Jauges(0, 2, 1, 0)),
            new Colonie(9, 10, new Jauges(1, 0, 0, 1))
    };
    public static Colonie[] C = new Colonie[] {
            new Colonie(15, 8, new Jauges(1, 1, 2, 0)),
            new Colonie(15, 8, new Jauges(2, 0, 1, 1)),
            new Colonie(15, 8, new Jauges(0, 1, 1, 2)),
            new Colonie(15, 13, new Jauges(1, 0, 1, 0)),
            new Colonie(15, 13, new Jauges(1, 1, 0, 0))
    };
    public static Colonie[] D = new Colonie[] {
            new Colonie(21, 11, new Jauges(0, 2, 3, 0)),
            new Colonie(21, 11, new Jauges(3, 0, 0, 2)),
            new Colonie(21, 11, new Jauges(0, 0, 2, 3)),
            new Colonie(21, 16, new Jauges(0, 1, 1, 0)),
            new Colonie(21, 16, new Jauges(1, 0, 0, 1))
    };
    public static Colonie[] E = new Colonie[] {
            new Colonie(30, 20, new Jauges(0, 0, 3, 2)),
            new Colonie(30, 20, new Jauges(0, 0, 2, 3)),
            new Colonie(30, 22, new Jauges(0, 0, 0, 4)),
            new Colonie(30, 24, new Jauges(0, 0, 0, 3)),
            new Colonie(30, 24, new Jauges(0, 0, 3, 0))
    };

    private boolean recto = true; // true = recto -> pillage, false = verso -> alliance

    private int pillage;
    private int alliance;

    private final int condition;

    private int pointDeVictoirePillage;
    private final int pointDeVictoireAlliance;

    private final Jauges jauge;

    private Colonie(int condition, int pointDeVictoireAlliance, Jauges jauge) {
        this.condition = condition;
        switch (condition) {
            case 3:
                pillage = 3;
                alliance = 1;
                pointDeVictoirePillage = 2;
                break;
            case 9:
                pillage = 4;
                alliance = 1;
                pointDeVictoirePillage = 5;
                break;
            case 15:
                pillage = 5;
                alliance = 2;
                pointDeVictoirePillage = 8;
                break;
            case 21:
                pillage = 7;
                alliance = 2;
                pointDeVictoirePillage = 11;
                break;
            case 30:
                pillage = 12;
                alliance = 4;
                pointDeVictoirePillage = 16;
                break;
        }
        this.pointDeVictoireAlliance = pointDeVictoireAlliance;
        this.jauge = jauge;
    }

    public void setRecto(boolean recto) {
        this.recto = recto;
    }

    public boolean isRecto() {
        return recto;
    }

    public int getPillage() {
        return pillage;
    }

    public int getAlliance() {
        return alliance;
    }

    public int getCondition() {
        return condition;
    }

    public int getPointDeVictoirePillage() {
        return pointDeVictoirePillage;
    }

    public int getPointDeVictoireAlliance() {
        return pointDeVictoireAlliance;
    }

    public Jauges getJauge() {
        return jauge;
    }
}
