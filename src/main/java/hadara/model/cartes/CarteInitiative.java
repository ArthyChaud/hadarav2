package hadara.model.cartes;

import hadara.model.Jauges;
import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.Random;

public enum CarteInitiative implements Packable {
    _1(1, 8, new Jauges(3, 2, 1, 4), 8, new Jauges(4, 1, 1, 4)),
    _2(2, 8, new Jauges(2, 1, 3, 4), 8, new Jauges(2, 1, 4, 3)),
    _3(3, 8, new Jauges(3, 2, 2, 3), 8, new Jauges(2, 3, 3, 2)),
    _4(4, 9, new Jauges(2, 1, 2, 5), 10, new Jauges(2, 2, 1, 4)),
    _5(5, 9, new Jauges(2, 2, 1, 9), 11, new Jauges(1, 1, 1, 6));

    public static CarteInitiative[] carteInitiatives;

    static {
        loadCarteInitiative();
    }

    private final int initiative;

    private int monnaie;
    private Jauges jauge;

    private final int monnaieA;
    private final Jauges jaugeA;
    private final int monnaieB;
    private final Jauges jaugeB;

    private char currentFace;

    CarteInitiative(int initiative, int monnaieA, Jauges jaugeA, int monnaieB, Jauges jaugeB) {
        this.initiative = initiative;
        this.monnaieA = monnaieA;
        this.jaugeA = jaugeA;
        this.monnaieB = monnaieB;
        this.jaugeB = jaugeB;
        setRandomFace(_Random.RAND);
    }

    public void setRandomFace(@NotNull Random rand) {
        if (rand.nextBoolean()) {
            currentFace = 'A';
            monnaie = monnaieA;
            jauge = new Jauges(jaugeA);
        } else {
            currentFace = 'B';
            monnaie = monnaieB;
            jauge = new Jauges(jaugeB);
        }
    }

    public static void setRAND(Random RAND) {
        _Random.RAND = RAND;
    }

    public static void loadCarteInitiative() {
        carteInitiatives = new CarteInitiative[] {_1, _2, _3, _4, _5};
    }

    public int getInitiative() {
        return initiative;
    }

    public int getMonnaie() {
        return monnaie;
    }

    public Jauges getJauge() {
        return jauge;
    }

    public char getCurrentFace() {
        return currentFace;
    }
}

abstract class _Random { // Just because we need the RAND attribute to be instancied before the enumerations
    public static Random RAND = new Random(Calendar.getInstance().getTimeInMillis());
}