package hadara.model;
import java.util.*;

public class Plateau {
  public static final PlateauPart[] ALL_PARTS = new PlateauPart[] {
      new PlateauPart("Vert"),
      new PlateauPart("Rouge"),
      new PlateauPart("Bleu"),
      new PlateauPart("Jaune"),
      new PlateauPart("Violet")
  };

  private PlateauPart[] parts = new PlateauPart[5];

  public Plateau() {
      List<PlateauPart> buffer = Arrays.asList(ALL_PARTS);
      Collections.shuffle(buffer);
      for (int i = 0; i < 5; i++)
        parts[0] = buffer.get(i);
  }

  public PlateauPart[] getParts() {
    return parts;
  }
}


