package hadara.model;

import hadara.model.cartes.CarteInitiative;

import java.util.*;
import java.util.stream.Collectors;

public class Partie {
    private static Partie instance;

    private List<Joueur> joueurs;

    private Plateau plateau;

    public Partie() {
        instance = this;
    }

    public void setGame(Collection<Joueur> joueurs) {
        this.joueurs = new ArrayList<>(joueurs);
        List<CarteInitiative> carteInitiatives =
                Arrays.stream(CarteInitiative.carteInitiatives)
                        .filter(c -> c.getInitiative() <= this.joueurs.size())
                        .collect(Collectors.toList());
        Collections.shuffle(carteInitiatives);
        for (int i = 0; i < joueurs.size(); i++)
            this.joueurs.get(i).setCarteInitiative(carteInitiatives.get(i));
        plateau = new Plateau();
    }

    public static Partie getInstance() {
        return instance;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public List<Joueur> getJoueurs() {
        return joueurs;
    }
}
