package hadara.controller;

import hadara.App;
import hadara.model.Joueur;
import hadara.model.Partie;
import hadara.view.PartieView;
import hadara.view.View;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;

import java.util.ArrayList;
import java.util.List;

public class MenuJouerController extends BasicController {

  @FXML
  public ChoiceBox<String> choiceBox;

  public MenuJouerController() {
    super();
  }

  public MenuJouerController(Partie model, View view) {
    super(model, view);
  }

  public void start() {
    App application = App.getInstance();
    List<Joueur> joueurs = new ArrayList<>();
    application.setCurrentView(new PartieView());
    for (int i = 0; i < Integer.parseInt(choiceBox.getValue()); i++)
      joueurs.add(new Joueur());
    application.getModel().setGame(joueurs);
  }
}
