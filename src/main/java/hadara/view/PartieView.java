package hadara.view;

import javafx.stage.Stage;

public class PartieView extends View {
  @Override
  public void start(Stage stage) {

    super.start(stage, "/templates/PartieView.fxml");
    stage.setMaximized(true);
  }
}
