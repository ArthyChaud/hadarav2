package hadara.model.cartes;

import hadara.model.Jauges;

public class Carte implements Packable {

    //Argent gagnée en défaussant définitivement
    private final int deleteReward;
    // Prix d'achat de carte
    private final int cardPrice;
    // Points de victoire
    private final int victoryPoints;

    private final Jauges jauges;

    public Carte(){
        this(0,0,0, new Jauges());
    }

    // si les points ne figurent pas sur la carte mettre 0 dans les pt concernées
    public Carte(int cardPrice, int deleteReward, int victoryPoints, Jauges jauges){
        this.cardPrice = cardPrice;
        this.deleteReward = deleteReward;
        this.victoryPoints = victoryPoints;
        this.jauges = jauges;
    }

    // Pas besoin de setters vu que la carte crée n'a pas besoin de changement, il faut respecter le constructeur

    public int getDeleteReward() {
        return deleteReward;
    }

    public int getCardPrice() {
        return cardPrice;
    }

    public int getVictoryPoints() {
        return victoryPoints;
    }

    public Jauges getJauges() {
        return jauges;
    }
}
