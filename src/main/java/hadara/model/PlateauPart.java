package hadara.model;

public class PlateauPart {
  public String couleur;

  public PlateauPart(String couleur) {
    this.couleur = couleur;
  }

  @Override
  public String toString() {
    return "Plateau" + couleur;
  }
}
