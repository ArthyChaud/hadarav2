package hadara;
import hadara.view.MenuPrincipalView;
import hadara.view.View;
import javafx.application.Application;
import hadara.model.Partie;
import javafx.stage.Stage;

public class App extends Application {
    private static App instance;

    private Stage stage;
    private Partie model;
    private View currentView;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        instance = this;
        stage = primaryStage;
        model = new Partie();
        setCurrentView(new MenuPrincipalView());
    }

    public static App getInstance() {
        return instance;
    }

    public View getCurrentView() {
        return currentView;
    }

    public void setCurrentView(View currentView) {
        this.currentView = currentView;
        currentView.start(stage);
    }

    public Partie getModel() {
        return model;
    }
}

