package hadara.view;

import hadara.model.Partie;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

public abstract class View {
    public static View instance;

    public static Pane root;

    public Partie model;

    public View() {
        instance = this;
    }

    public abstract void start(Stage stage);
    public void start(Stage stage, String ressource){
        try {
            root = FXMLLoader.load(
                    getClass().getResource(ressource)
            );
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}