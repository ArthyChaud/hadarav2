package hadara.controller;

import hadara.model.Partie;
import hadara.view.View;

public abstract class BasicController {
  protected Partie model;
  protected View view;

  public BasicController() {}

  public BasicController(Partie model, View view) {
    this.model = model;
    this.view = view;
    this.view.model = model;
  }
}
