package hadara.model;

import org.jetbrains.annotations.NotNull;

public class Jauges {
    private int revenu;
    private int militaire;
    private int culture;
    private int alimentation;

    public Jauges() {
        this(0, 0, 0, 0);
    }

    public Jauges(@NotNull Jauges j) {
        this(j.revenu, j.militaire, j.culture, j.alimentation);
    }

    public Jauges(int revenu, int militaire, int culture, int alimentation) {
        this.revenu = revenu;
        this.militaire = militaire;
        this.culture = culture;
        this.alimentation = alimentation;
    }

    public void add(@NotNull Jauges j) {
        revenu += j.revenu;
        militaire += j.militaire;
        culture += j.culture;
        alimentation += j.alimentation;
    }

    public int getRevenu() {
        return revenu;
    }

    public void setRevenu(int revenu) {
        this.revenu = revenu;
    }

    public void addRevenu(int revenu) {
        this.revenu += revenu;
    }

    public int getMilitaire() {
        return militaire;
    }

    public void setMilitaire(int militaire) {
        this.militaire = militaire;
    }

    public void addMilitaire(int militaire) {
        this.militaire += militaire;
    }

    public int getCulture() {
        return culture;
    }

    public void setCulture(int culture) {
        this.culture = culture;
    }

    public void addCulture(int culture) {
        this.culture += culture;
    }

    public int getAlimentation() {
        return alimentation;
    }

    public void setAlimentation(int alimentation) {
        this.alimentation = alimentation;
    }

    public void addAlimentation(int alimentation) {
        this.alimentation += alimentation;
    }
}
