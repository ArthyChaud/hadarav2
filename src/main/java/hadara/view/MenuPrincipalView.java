package hadara.view;

import javafx.stage.Stage;

public class MenuPrincipalView extends View {
  @Override
  public void start(Stage stage) {
    super.start(stage, "/templates/MenuPrincipalView.fxml");
  }
}
