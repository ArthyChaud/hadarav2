package hadara.model;

import hadara.model.Joueur;
import hadara.model.Partie;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PartieUnitTest {

    @Tag("include")
    @Test
    public void testDistributionCartesInitiatives() {
        int nb_joueurs = 4;
        List<Joueur> joueurs = new ArrayList<>();
        for (int i = 0; i < nb_joueurs; i++)
            joueurs.add(new Joueur());
        Partie partie = new Partie();
        partie.setGame(joueurs);
        joueurs = partie.getJoueurs();
        List<Boolean> initiatives = new ArrayList<>();
        for (int i = 0; i < nb_joueurs; i++)
            initiatives.add(false);
        for (Joueur joueur : joueurs) {
            initiatives.set(joueur.getCarteInitiative().getInitiative() - 1, true);
        }
        System.out.println(initiatives);
        assertEquals(0, initiatives.stream().filter(b -> !b).count());
    }
}
