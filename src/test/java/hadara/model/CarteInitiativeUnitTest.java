package hadara.model;

import hadara.model.cartes.CarteInitiative;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CarteInitiativeUnitTest {
    @Tag("include")
    @Test
    public void testConstruireCarte() {
        for (int i = 0; i < CarteInitiative.carteInitiatives.length; i++)
            assertEquals(CarteInitiative.carteInitiatives[i].getInitiative(), i + 1);
    }

    @Tag("include")
    @Test
    public void testSetRandomFace() {
        Random rand = Mockito.mock(Random.class);
        Mockito.when(rand.nextBoolean()).thenReturn(true, false);
        CarteInitiative._5.setRandomFace(rand);
        assertEquals(9, CarteInitiative._5.getMonnaie());
        CarteInitiative._5.setRandomFace(rand);
        assertEquals(11, CarteInitiative._5.getMonnaie());
    }
}
